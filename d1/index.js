console.log("Hello World!")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
// + - * / %

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

//Subtraction/Multiplication/division assignment operator (-=, *=, /=)

assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

//Multiple Operators and Parenthesis
/*
-when multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
- The operations were done in the following order:
1. 3 * 4 = 12
2. 12 / 5 = 2.4
3. 1 + 2 = 3
4. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

//Increment and Decrement Operator
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation.
//The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment".
++z;
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

//post-fix incrementation
//The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
z++;
console.log(z); //3 - The value of z was added with 1
console.log(z++); // the previous value of the variable is returned.
console.log(z); //4 a new value is now returned.

//pre-fix vs. post-fix incrementation
console.log(z++); //4
//console.log(z++); //5
//console.log(z++); //6
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately

//pre-fix and post-fix decrementation
console.log(--z); //5 - with pre-fix decremetation the result of subtraction by 1 is returned immediately

console.log (z--); //5 - with post-fix decrementation the result of subtraction by q is not immediately returned, instead the previous value is returned first
console.log(z); //4

//Type Coercion
//is the automatic or implicit conversion of values from one data type to another
let numA = '10' ;
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);

//Adding/Concatenating a string and a number will result to a string

let numC = 16; 
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

//the boolean "true" is also associated with the value of 1
let numE = true + 1; 
console.log(numE); //
console.log(typeof numE); //number

//the boolean "false" is also associated with the value of 0

let numF = false + 1;
console.log(numF); //number


//Comparison Operators
let juan = 'juan';

//(==) Equality Operator (checks whether the operands are equal/have the same content)
//Attempts to CONVERT AND COMPARE operands of different data types
//Returns a boolean value

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 =="1"); //true
console.log(0 == false) //true
console.log('juan' == 'JUAN'); //false, case sensitive;
console.log('juan' == juan); //true

//(!=) Inequality Operator
//Checks whether the operands are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1') //false
console.log(0 != false); //false
console.log('juan' != 'JUAN'); //true, case sensitive
console.log('juan' != juan); //false

// (===) Strictly Equality Operator
console.log('Strictly Equality Operator')
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false //type was checked, not the same data type
console.log(0 === false) //false //type was checked, not the same data type
console.log('juan' === 'JUAN'); //false, case sensitive;
console.log('juan' === juan); //true

// (!==) Strictly Inequality Operator
console.log('Strictly Inequality Operator')
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1') //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN'); //true, case sensitive
console.log('juan' !== juan); //false

//Relational Comparison Operators
//Check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

//Greater than (>)
console.log("Greater Than")
console.log(x > y); //false
console.log(w > y); //true

//Less than   (<)
console.log("Less Than")
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion - to change string to number
console.log(numString < 6000); //true - forced coercion - to change string to number
console.log(numString < "Jose"); //true - '5500' < "Jose"